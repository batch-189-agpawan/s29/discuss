db.inventory.insertMany([
{
	"name": "JavaScript for Beginners",
	"author": "James Doe",
	"price": 5000,
	"stocks": 50,
	"publisher": "JS Publishing House"
},
{
	"name": "HTML and CSS",
	"author": "John Thomas",
	"price": 5000,
	"stocks": 38,
	"publisher": "NY Publishers"
},
{
	"name": "Web Development Fundamentals",
	"author": "Noah Jimenez",
	"price": 3000,
	"stocks": 10,
	"publisher": "Big Idea Publishing"
},
{
	"name": "Java Programming",
	"author": "David Michael",
	"price": 10000,
	"stocks": 100,
	"publisher": "JS Publishing House"
}

	])

// Comparison Query Operators
// $gt?$gte.operator
/*
	Syntax:
		db.collection.find({"field": { $gt: value}})
		db.collection.find({"field": { $gte: value}})

*/


db.inventory.find({
	"stocks": {
		$gt : 50
	}
})

db.inventory.find({
	"stocks": {
		$gte : 50
	}
})


// let 





db.inventory.find({
	"stocks": {
		$lt: 50
	}
})

db.inventory.find({
	"stocks": {
		$lte: 50
	}
})


// $ne operator
/*
	syntax
		db.collectionName.find({field: {$ne: value}})
*/


db.inventory.find({
	"stocks": {
		$ne: 50
	}
})

// $eq operator
/*
	Syntax
		db.collectionName.find({field : {$eq: value}})
*/

db.inventory.find({
	"stocks": {
		$eq: 50
	}
})


// $in  Operator
/*
	syntax
		db.collectionName.find({ field: { $in:[value1, value2] }})
*/

db.inventor.find({
	"price": {
		$in: [10000,5000]
	}
})

// $nin
/*
	Syntax
		db.collectionName.find({ field : {$nin: [value1,value2]}})
*/

db.inventory.find({
	"price": {
		$nin: [10000, 5000]
	}
})

/*

db.inventory.find({
	"price": {$gt: 2000} && {$lt:4000}
})
*/

/*
	Mini-Activity
		1. in the inventory collection, return all the documents that have the price equal or less than 4000.
		2.In the inventory collection, return all the documents that have stocks of 50 and 100

*/

db.invenotry.find({
	"price": {$lte : 4000}
})

db.inventory.find({
	"stocks": {
		$in:[50,100]
	}
})


// L:ogical querty operators
/*
$or operator
	syntax
		db.collectionName.find({
	$or: [
		{fieldA : valueA},
		{fieldB : valueB}]
		})
*/

db.inventory.find({
	$or[
	{ "name": "HTML and CSS"},
	{"publisher": "JS Publishing House"}]
})

db.inventory.find({
	$or: [
	{"author": "James Doe"},
	{"price": {
		$lte: 5000
	}}]
})

/*
$and operator

	syntax:
		db.collectionName.find({ $and: [ {fieldA: valueA}, {fieldB: valueB} ]})

*/

db.inventory.find({
	$and: [
	{
		"stocks":{
			$ne:50
		}

	},	{
		"price":{
			$ne:5000
		},
	
	}]
})

// by default (same sa taas)
db.inventory.find({
	"stocks":{
		$ne: 50
	},
	{
	"price": {
		$ne:5000
	}
	
	}
})

//Field Projection
//Inclusion
/*
	Syntax
		db.collectionName.find({criteria}, {field: 1} )

*/


db.inventory.find({
	"publisher": "JS Publishing House"
	},
	{ "name": 1,
	 "author": 1,
	 "price": 1

	})


// Exclusion
/*
	Syntax
		db.collectionName.find({criteria), {field:0})
*/

db.inventory.find(
{
	"author": "Noah Jimenez"
},
{
	"price": 0,
	"stocks": 0
})

db.inventory.find(
{
	"price": {
		$lte: 5000}
	},
	{
		"_id": 0,
		"name": 1,
		"author": 1
	}
	)

// Evaluation query operator
// $regex operator
/*
	Syntax:
		db.collectionName.find({field: {$regex: 'pattern', $options: 'optionsValue'}})
*/


// CASE SENSITIVE
db.inventory.find({
	"author": {
		$regex: 'M'
	}
})

// CASE INSENSITIVE
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
})

// Can query operators be used on operations such as updateOne, updateMany, deleteOne, deleteMany?

/*
YES.
updateOne({criteria/query}, {$set})
updateMany({criteria/query}, {$set})
deleteOne({criteria/query})
deleteMany({criteria/query})

*/